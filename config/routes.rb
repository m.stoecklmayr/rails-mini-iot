Rails.application.routes.draw do
  root "locations#index"

  resources :locations do
    resources :devices
  end
  resources :device_models
  resources :data_points, only: [:index, :create, :show] do
    resources :visualisations, only: :show
    resources :data_points_device, only: :show
  end
end
