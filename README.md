# Example Rails Application

This mini IOT application allows the user to configure device models and devices for a given location.
Each device model can have multiple data point definitions which are configurable.
For each device, the respective data point can be set. This is done via a POST request to /data_point . An example curl
request can be found below.
For simplicity, the data point definitions have been hard coded and can be seen at the device model overview (/device_models)


###Technical Details:
- Ruby 3.0.2
- Rails 6.1.4
- Postgres 13

### Development Details
To start the local server, run `rails server`. It runs per default on port 3000 and redirects to the index page of the `locations` controller.
 
To run it using docker-compose
- Builds and starts the images ` docker-compose up`
- Setup DB `docker-compose run web rake RAILS_ENV=compose db:create`
- Insert hard coded entries `docker-compose run web rake RAILS_ENV=compose db:seed`

### Tests Details
Test files have been created through the `rails generate` commands, but have no been implemented yet

### CI Integration
The application uses a basic version of a CI file. At the moment, it only does a bundle install as I do not yet
know which tools / frameworks to use for other steps as e.g. unit or integration tests

### Example Curl Request
The device id and the definition id has to be valid. The respective ids can be seen at the details for a location (/location/{id}) and at the device model overview (/device_models).
```json
curl --location --request POST 'localhost:3000/data_points' \
--header 'Content-Type: application/json' \
--data-raw '{
    "device_id": "2",
    "definition_id": "2",
    "value":"15"
    }'
```

You can also fetch data points per device via:
`GET  localhost:3000/data_points/1`

The naming of the API has been unlucky. The first id is the device id and the second one the data definition id.
I realized it but did not do all the changes for it for now.
`GET  localhost:3000/data_points/1/data_points_device/1`