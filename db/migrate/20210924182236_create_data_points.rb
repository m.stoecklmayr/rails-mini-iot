class CreateDataPoints < ActiveRecord::Migration[6.1]
  def change
    create_table :data_points do |t|
      t.integer :device_id
      t.integer :definition_id
      t.numeric :value

      t.timestamps
    end
  end
end
