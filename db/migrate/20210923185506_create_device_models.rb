class CreateDeviceModels < ActiveRecord::Migration[6.1]
  def change
    create_table :device_models do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
