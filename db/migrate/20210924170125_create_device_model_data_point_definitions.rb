class CreateDeviceModelDataPointDefinitions < ActiveRecord::Migration[6.1]
  def change
    create_table :device_model_data_point_definitions do |t|
      t.integer :device_model_id, null: false
      t.integer :data_point_definition_id, null: false

      t.timestamps
    end
  end
end
