DataPointDefinition.destroy_all
DataPointDefinition.create!([{name: 'Energy-Consumption', unit: 'kW/h'},{name: 'Energy-Production', unit: 'kW/h'},{name: 'Temperature', unit: 'C'}])
p "Created #{DataPointDefinition.count} data point definitions"