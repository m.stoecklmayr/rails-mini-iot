class Device < ApplicationRecord
  belongs_to :location
  belongs_to :device_model
end
