class DataPointDefinition < ApplicationRecord
  has_many :device_model_data_point_definitions
  has_many :device_models, through: :device_model_data_point_definitions
end
