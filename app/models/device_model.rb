class DeviceModel < ApplicationRecord
  has_many :devices
  has_many :device_model_data_point_definitions
  has_many :data_point_definitions, through: :device_model_data_point_definitions
end
