class DeviceModelDataPointDefinition < ApplicationRecord
  belongs_to :device_model
  belongs_to :data_point_definition
end
