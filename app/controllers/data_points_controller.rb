class DataPointsController < ApplicationController
  protect_from_forgery with: :null_session

  def index
    render json: DataPoint.all
  end

  def show
    # fetch all entries via the device id
    data_point = DataPoint.where(device_id: params[:id])
    if data_point.nil?
      render json: data_point, status: :not_found
    else
      render json: data_point, status: :ok
    end
  end

  def create
    # validation logic should be put in extra method but did not figure out how to exit in there
    begin
      dev = Device.find(data_point_params[:device_id])
      definition = DataPointDefinition.find(data_point_params[:definition_id])
    rescue ActiveRecord::RecordNotFound
      # log error message here
    end
    if dev.nil? || definition.nil?
      # error handling could be more explicit
      render json: data_point_params, status: :unprocessable_entity
    else
      data_point = DataPoint.new(data_point_params)
      if data_point.save
        render json: data_point, status: :created
      else
        render json: data_point.errors, status: :unprocessable_entity
      end
    end
  end

  private
  def data_point_params
    params.require(:data_point).permit(:device_id, :definition_id, :value)
  end
end
