class DevicesController < ApplicationController
  def show
    #:location_id is 1/1 for the first 1/2 for the second
    # there is probably a smarter way to do it
    device_url_part = params[:location_id].split("/")[1]
    @device = Device.find(device_url_part)

    # I assume this business logic needs to be placed somewhere else
    @latest_data_points_hash = Hash.new
    @device.device_model.device_model_data_point_definitions.each do |definition|
      data_def_id = definition.data_point_definition.id
      latest = DataPoint.where("device_id = :dev_id and definition_id = :def_id", {dev_id: @device.id, def_id: data_def_id}).last
      unless latest.nil?
        dataPointName = DataPointDefinition.find(data_def_id).name
        @latest_data_points_hash[dataPointName] = latest
      end
    end
  end

  def create
    @location = Location.find(params[:location_id])
    @device = @location.devices.create(device_params)
    redirect_to location_path(@location)
  end

  def destroy
    @location = Location.find(params[:location_id])
    @device = @location.devices.find(params[:id])
    @device.destroy
    redirect_to location_path(@location)
  end

  private
  def device_params
    params.require(:device).permit(:name, :description, :device_model_id)
  end
end
