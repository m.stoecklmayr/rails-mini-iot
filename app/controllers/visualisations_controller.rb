class VisualisationsController < ApplicationController
  def show
    device_id = params[:id]
    data_point_definition_id = params[:data_point_id]

    # fixme: I realized at a late point that this should call the internal API instead
    @data_points = DataPoint.where("device_id = :dev_id and definition_id = :def_id", {dev_id: device_id, def_id: data_point_definition_id})
    @data_point_name = DataPointDefinition.find(data_point_definition_id).name
    print @data_points
  end
end
