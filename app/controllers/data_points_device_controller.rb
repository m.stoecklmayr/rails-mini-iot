class DataPointsDeviceController < ApplicationController
  def show
    # fetch all entries via the device id
    data_point = DataPoint.where(definition_id: params[:id], device_id: params[:data_point_id])
    if data_point.nil?
      render json: data_point, status: :not_found
    else
      render json: data_point, status: :ok
    end
  end

end
