# frozen_string_literal: true
class DeviceModelsController < ApplicationController
  def index
    @device_models = DeviceModel.all
    @data_point_definitions = DataPointDefinition.all
  end

  def load_data
    @device_model = DeviceModel.find(params[:id])
    @definitions = []
    @device_model.device_model_data_point_definitions.each do |dev_model_data_point_def|
      # find per data point is not the best idea, also the .name at the end
      @definitions.push(DataPointDefinition.find(dev_model_data_point_def.data_point_definition.id).name)
    end
  end

  def show
    load_data
  end

  def new
    @device_model = DeviceModel.new
    @data_point_definitions = DataPointDefinition.all
    @device_model_data_point_definition = @device_model.device_model_data_point_definitions.build
  end

  def create
    @device_model = DeviceModel.new(device_model_params)

    #do the cross table referencing of device models and data point definition
    params[:data_point_definition][:id].each do |data_point|
      unless data_point.empty?
        @device_model.device_model_data_point_definitions.build(data_point_definition_id: data_point)
      end
    end

    if @device_model.save
      #makes new request to device_model
      redirect_to @device_model
    else
      render :new
    end
  end

  def edit
    @data_point_definitions = DataPointDefinition.all
    load_data
  end

  def update
    @device_model = DeviceModel.find(params[:id])

    if @device_model.update(device_model_params)
      redirect_to @device_model
    else
      render :edit
    end
  end

  def destroy
    @device_model = DeviceModel.find(params[:id])
    @device_model.destroy

    redirect_to root_path
  end

  private
  def device_model_params
    params.require(:device_model).permit(:name, :description)
  end
end
